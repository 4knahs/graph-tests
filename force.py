#!/usr/bin/env python
import json

#import flask
import networkx as nx
from networkx.readwrite import json_graph


G = nx.gn_graph(1000)

for n in G:
    G.nodes[n]['name'] = n

d = json_graph.node_link_data(G)  # node-link format to serialize

#pos = nx.spring_layout(G)
#print(pos)

#for n in pos:
#	G.nodes[n]['X'] = pos[n][0]
#	G.nodes[n]['Y'] = pos[n][1]

json.dump(d, open('force.json', 'w'))
print('Wrote node-link JSON data to force/force.json')

nx.write_gexf(G, 'force.gexf')
print('Wrote node-link GEXF data to force/force.gexf')
