var width = 500,
    height = 500;
var xCenter = width / 2,
    yCenter = height / 2;

    

//########################Enable only one at a time!!
//fastForce();
//force();
//sigmaSimple();

// With real tappas data:

realSigma();
realD3Force();
//#####################################################

function sigmaSimple(){

  sigma.parsers.gexf('miserables.gexf',  { 

      // Here is the ID of the DOM element that
      // will contain the graph:
      container: 'chart-sigma',
      settings: {
        defaultNodeColor: '#ec5148', 
        defaultNodeSize: 30
      }
    },
    function(s) {
      // This function will be executed when the
      // graph is displayed, with "s" the related
      // sigma instance.
      console.log("Graph is displayed!");
      console.log(s.graph.nodes);
    }
  )
}

function realSigma(){
  $.getJSON('http://tappas.io:4000/dmdump/inclusive_methods/graph?session_id=pimiento@163.172.70.208-68&app_name=com.adsk.sketchbook', 
    (json) => {
      
      console.log("got json")

      es_graph = json[0]

      s = new sigma('chart-sigma');

      ids = []
      edges = 0

      max = es_graph.nodes.reduce((acc, val) => Math.max(acc, val.exclusive), 0)

      es_graph.nodes.forEach((node) => {
        if (node.method_name.startsWith("com.adsk")){
          //console.log(node.method_name)
          ids.push(node.method_index)
          s.graph.addNode(
            {
            "id": "" + node.method_index,
            "label": node.method_name,
            "x": Math.floor((Math.random() * width) + 1),
            "y": Math.floor((Math.random() * height) + 1),
            "size": Math.ceil(node.exclusive * 10 / max),
            }
          )
          }
        }
      );

      es_graph.edges.forEach((edge,index) =>
      {
        if(ids.includes(edge.s) && ids.includes(edge.d)){
          s.graph.addEdge(
            {
            "id": "" + index,
            "source": "" + edge.s,
            "target": "" + edge.d
            }
          )
          edges += 1
        }
      });

      console.log("#nodes = " + ids.length + " / " + es_graph.nodes.length)
      console.log("#edges = " + edges + " / " + es_graph.edges.length)

      s.settings({
        //clone: false,
        labelThreshold: 1000, //this hides the label
        //drawLabels: false,
        edgeColor: 'default',
        defaultEdgeColor: '#999',
        defaultNodeColor: '#ec5148'
        //defaultNodeSize: 50
      });

      //s.refresh();

      s.startForceAtlas2(
        {
          autoSettings: false,
          linLogMode: false,
          outboundAttractionDistribution: false,
          adjustSizes: false,
          edgeWeightInfluence: 0,
          scalingRatio: 1,
          strongGravityMode: false,
          gravity: 20,
          jitterTolerance: 1,
          barnesHutOptimize: false,
          barnesHutTheta: 1.2,//1.2,
          speed: 20,
          outboundAttCompensation: 1,
          totalSwinging: 1,
          totalEffectiveTraction: 0,
          complexIntervals: 500,
          simpleIntervals: 1000
        }
        )

      console.log("post atlas")

      //setTimeout(()=>{console.log("stopping atlas"); s.stopForceAtlas2();}, 30000);

      //function stopAtlas(){

      //s.startNoverlap();
    //  }
  })  
}

function fastForce(){

  d3.json("force.json", function(error, graph) {

    var canvas = d3.select("canvas")
        .attr("width", width)
        .attr("height", height);

    var force = d3.layout.force()
        .size([width*0.9, height*0.9]);

    //https://bl.ocks.org/mbostock/3180395
      if (error) throw error;

      var context = canvas.node().getContext("2d");

      force
          .nodes(graph.nodes)
          .links(graph.links)
          .on("tick", tick)
          .start();

      console.log("nodes = " + graph.nodes.length);
      console.log("links = " + graph.links.length);
    
      function tick() {
        context.clearRect(0, 0, width, height);

        let max = graph.nodes.reduce((a,b) => [Math.max(a[0], b.px), Math.max(a[1], b.py)], [0,0])
        let max_x = max[0]
        let max_y = max[1] 

        scale = 0.5 / Math.max(max_x / width, max_y / height)

        // draw links
        context.strokeStyle = "#ccc";
        context.beginPath();
        graph.links.forEach(function(d) {
          context.moveTo(xCenter + d.source.x * scale, yCenter + d.source.y * scale);
          context.lineTo(xCenter + d.target.x * scale, yCenter + d.target.y * scale);
        });
        context.stroke();

        // draw nodes
        context.fillStyle = "steelblue";
        context.beginPath();
        graph.nodes.forEach(function(d) {
          context.moveTo(xCenter + d.x * scale , yCenter + d.y * scale);
          context.arc(xCenter + d.x * scale , yCenter + d.y * scale, 4.5 * scale, 0, 2 * Math.PI);
        });
        context.fill();
      }
  });
}

function realD3Force(){

  $.getJSON('http://tappas.io:4000/dmdump/inclusive_methods/graph?session_id=pimiento@163.172.70.208-68&app_name=com.adsk.sketchbook', 
    (es_json) => {
      
      console.log("got json")
      es_graph = es_json[0]

      node_translation = {}

      my_nodes = es_graph.nodes
        .filter((node) => node.method_name.startsWith("com.adsk"))
        .map((node, index) => {
          // We need to recalculate the indexes for the links
          // This is because these indexes are used to access the node array!!
          node_translation[node.method_index] = index
          return {
            "id": index, //node.method_index,
            "name": node.method_name
        }});

      console.log("processed nodes:")
      console.log(my_nodes)

      console.log("original edges:")
      console.log(es_graph.edges)

      links = es_graph.edges
        //.filter((link) => (ids.includes(link.s) && ids.includes(link.d)))
        .filter((link) => (node_translation[link.s] != null && node_translation[link.d] != null))
        .map((link, index) => { 
          return {
            "source": node_translation[link.s], //link.s,
            "target": node_translation[link.d]  //link.d
        }});

      console.log("new edges")
      console.log(my_links)

      console.log("" + nodes.length + " / " + es_graph.nodes.length + " Links " + links.length + " / " + es_graph.edges.length)

      json = {
        "directed": true, 
        "graph": {},
        "nodes": my_nodes,
        "links": my_links,
        "multigraph": false
      }

    console.log(json)

    var fill = d3.scale.category20();

    var vis = d3.select("#chart-d3")
      .append("svg:svg")
        .attr("width", width)
        .attr("height", height);

    var force = d3.layout.force()
        .charge(-120)
        .linkDistance(30)
        .nodes(json.nodes)
        .links(json.links)
        .size([width, height])
        .start();

    var link = vis.selectAll("line.link")
        .data(json.links)
        .enter().append("svg:line")
        .attr("class", "link")
        .style("stroke-width", function(d) { return Math.sqrt(d.value); })
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    var node = vis.selectAll("circle.node")
        .data(json.nodes)
        .enter().append("svg:circle")
        .attr("class", "node")
        .attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; })
        .attr("r", 5)
        .style("fill", function(d) { return fill(d.group); })
        .call(force.drag);

    node.append("svg:title")
        .text(function(d) { return d.name; });

    vis.style("opacity", 1e-6)
        .transition()
        .duration(1000)
        .style("opacity", 1);

    force.on("tick", function() {
        let max = json.nodes.reduce((a,b) => [Math.max(a[0], b.px), Math.max(a[1], b.py)], [0,0])
        let max_x = max[0]
        let max_y = max[1] 

        scale = 0.5 / Math.max(max_x / width, max_y / height)

        //console.log(scale);

        link.attr("x1", function(d) { return xCenter + d.source.x * scale; })
            .attr("y1", function(d) { return yCenter + d.source.y * scale; })
            .attr("x2", function(d) { return xCenter + d.target.x * scale; })
            .attr("y2", function(d) { return yCenter + d.target.y * scale; });

        node.attr("cx", function(d) { return xCenter + d.x * scale; })
            .attr("cy", function(d) { return yCenter + d.y * scale; });
    });
  });
}

function force(){

  d3.json("tappas.json", function(error, json) {

    console.log(json)

    if (error) throw error;

    var fill = d3.scale.category20();

    var vis = d3.select("#chart-d3")
      .append("svg:svg")
        .attr("width", width)
        .attr("height", height);

    var force = d3.layout.force()
        .charge(-120)
        .linkDistance(30)
        .nodes(json.nodes)
        .links(json.links)
        .size([width, height])
        .start();

    var link = vis.selectAll("line.link")
        .data(json.links)
      .enter().append("svg:line")
        .attr("class", "link")
        .style("stroke-width", function(d) { return Math.sqrt(d.value); })
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    var node = vis.selectAll("circle.node")
        .data(json.nodes)
      .enter().append("svg:circle")
        .attr("class", "node")
        .attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; })
        .attr("r", 5)
        .style("fill", function(d) { return fill(d.group); })
        .call(force.drag);

    node.append("svg:title")
        .text(function(d) { return d.name; });

    vis.style("opacity", 1e-6)
      .transition()
        .duration(1000)
        .style("opacity", 1);

    force.on("tick", function() {
        let max = json.nodes.reduce((a,b) => [Math.max(a[0], b.px), Math.max(a[1], b.py)], [0,0])
        let max_x = max[0]
        let max_y = max[1] 

        scale = 0.5 / Math.max(max_x / width, max_y / height)

        //console.log(scale);

        link.attr("x1", function(d) { return xCenter + d.source.x * scale; })
            .attr("y1", function(d) { return yCenter + d.source.y * scale; })
            .attr("x2", function(d) { return xCenter + d.target.x * scale; })
            .attr("y2", function(d) { return yCenter + d.target.y * scale; });

        node.attr("cx", function(d) { return xCenter + d.x * scale; })
            .attr("cy", function(d) { return yCenter + d.y * scale; });
    });
  });
}