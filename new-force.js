function median(values){
    values.sort(function(a,b){
    return a-b;
  });

  if(values.length ===0) return 0

  var half = Math.floor(values.length / 2);

  if (values.length % 2)
    return values[half];
  else
    return (values[half - 1] + values[half]) / 2.0;
}

class SigmaCallGraph {
	constructor(container){
		this._container = container;
		this._width = document.getElementById(container).clientWidth;
		this._height = document.getElementById(container).clientHeight;  
    this._maxNodeSize = 5

    this.addExtraGraphMethods();

		this._sigma = new sigma(container);
    
		this._sigma.settings({
	        ////clone: false,
	        labelThreshold: 10, //this hides the label
	        ////drawLabels: false,
	        edgeColor: 'default',
	        defaultEdgeColor: '#999',
	        defaultNodeColor: '#004d40'
	        //defaultNodeSize: 50
      	});

    //CustomShapes.init(this._sigma);
    //this._sigma.refresh();

    this._filter = new sigma.plugins.filter(this._sigma)
	}

  setData(nodes, edges, options){
    this._nodes = nodes;
    this._edges = edges;

    this.addNodes(nodes, options);
    this.addEdges(edges);
  }

  labelOn(event){
    let node = event.data.node;
      
      //node.size = node.originalSize * 2;
      //node.color = "#76ff03"

    console.log("Setting popup: " + node.label)
    let popup = $('.sigma-popup');

      let left = node['cam0:x'];
      let top = node['cam0:y'];

      let spl = node.label.split('.')
      let mth = spl[spl.length - 1]
      let cls = spl[spl.length - 2]
      let pkg = node.label.substring(0, node.label.length - mth.length - cls.length - 2)

      let text = 
        //node.label + 
        "<b>Package:</b> " +
        pkg +
        "<br><b>Class:</b> " +
        cls +
        "<br><b>Method:</b> " +
        mth +
        "<br><b>Exclusive:</b> " + 
        (node.exclusive/1000) + " ms" +
        "<br><b>Inclusive:</b> " +
        (node.inclusive/1000) + " ms"

      popup.html(text)
      let theHeight = popup.height();
      let theWidth = popup.width();

      console.log("" + left + " " + top + " " + theHeight + " " + theWidth)

      popup.css('left', (left)-30 -theWidth+ 'px');
      popup.css('top', (top-(theHeight/2))+35 + 'px');
      popup.addClass('popup-visible')

      this._sigma.refresh();
  }

  labelOff(event){
    let node = event.data.node;
      
      //node.size = node.originalSize;
      //node.color = node.originalColor;
      console.log("hiding popup")
      $('.sigma-popup').removeClass('popup-visible');
      this._sigma.refresh();
  }

  zoomToNode(aNode){
    //let aNode = sigmaInstance.nodes()[0]
    let cam = this._sigma.camera 
    let pfx = cam.readPrefix
    sigma.utils.zoomTo(
      cam,                        // cam
      aNode[pfx + 'x'] - cam.x,   // x
      aNode[pfx + 'y'] - cam.y,   // y
      .5,                         // ratio
      {'duration': 1000}          // animation
    )
  }

  zoomToCoords(camX, camY){
    let cam = this._sigma.camera 
    let pfx = cam.readPrefix
    sigma.utils.zoomTo(
      cam,                        // cam
      camX - cam.x,   // x
      camY - cam.y,   // y
      1,                         // ratio
      {'duration': 1000}          // animation
    )
  }

  addUnhideNeighborsOnClick(){
    let s = this._sigma

    this._sigma.bind('clickNode', (e) => {
      let node = e.data.node;
      let toUnhide = s.graph.neighbors(node.id)
      let cam = s.camera 
      let pfx = cam.readPrefix

      console.log(node.label + " neighbors: " + toUnhide)
      let xs = [], xy = []
      
      this._sigma.graph.nodes().forEach((node) => 
        {
          node.color = node.originalColor

          if(toUnhide[node.id]){
            node.hidden = false;
            node.color = "#76ff03";
            xs.push(node[pfx + 'x']) // camera coords
            xy.push(node[pfx + 'y'])
          }
        }
      )

      let x_avg = xs.reduce((acc, val) => acc + val,0) / xs.length
      let y_avg = xy.reduce((acc, val) => acc + val, 0) / xy.length

      console.log("new camera : " + x_avg + ":" + y_avg)


      this.zoomToCoords(x_avg, y_avg);
      //this.unhideNodes(nodes);
      s.refresh();
      this.startForce(2000);
      //Zooms to middle of the nodes
      
    });
  }

  addLabelsOnClick(){
    let s = this._sigma

    this._sigma.bind('clickNode', (e) => {
      this.labelOn(e);
    });

    this._sigma.bind('clickStage', (e) => {
      this.labelOff(e);
    });
  }

  addLabelsOnHover(){
    let s = this._sigma

    this._sigma.bind('overNode', (e) => {
      this.labelOn(e);
    });

    this._sigma.bind('outNode', (e) => {
      this.labelOff(e);
    });
  }

	addExtraGraphMethods(){
		// Add a method to the graph model that returns an
		// object with every neighbors of a node inside:
		sigma.classes.graph.addMethod('neighbors', function(nodeId) {
			var k,
		        neighbors = {},
		        index = this.allNeighborsIndex[nodeId] || {};

		    for (k in index)
		      neighbors[k] = this.nodesIndex[k];

		    return neighbors;
		});
	}

  hideNodes(nodes){
    let ids = nodes.map((node) => node.id)

    this._sigma.graph.nodes(ids).forEach((node) => node.hidden = true)
  }

  unhideNodes(nodes){
    let ids = nodes.map((node) => node.id)

    this._sigma.graph.nodes(ids).forEach((node) => node.hidden = false)
  }

	enableNeighborFocus(){

	  var s = this._sigma

		// We first need to save the original colors of our
      // nodes and edges, like this:
      s.graph.nodes().forEach(function(n) {
        n.originalColor = n.color;
        n.originalSize = n.size
      });
      s.graph.edges().forEach(function(e) {
        e.originalColor = e.color;
        e.originalSize = e.size
      });

      // When a node is clicked, we check for each node
      // if it is a neighbor of the clicked one. If not,
      // we set its color as grey, and else, it takes its
      // original color.
      // We do the same for the edges, and we only keep
      // edges that have both extremities colored.
      s.bind('clickNode', function(e) {
        var nodeId = e.data.node.id,
            toKeep = s.graph.neighbors(nodeId);
        toKeep[nodeId] = e.data.node;

        s.graph.nodes().forEach(function(n) {
          if (toKeep[n.id])
            n.color = n.originalColor;
          else
            n.color = '#eee';
        });

        s.graph.edges().forEach(function(e) {
          if (toKeep[e.source] && toKeep[e.target])
            e.color = e.originalColor;
          else
            e.color = '#eee';
        });

        // Since the data has been modified, we need to
        // call the refresh method to make the colors
        // update effective.
        s.refresh();
      });

      // When the stage is clicked, we just color each
      // node and edge with its original color.
      s.bind('clickStage', function(e) {
        s.graph.nodes().forEach(function(n) {
          n.color = n.originalColor;
        });

        s.graph.edges().forEach(function(e) {
          e.color = e.originalColor;
        });

        // Same as in the previous event:
        s.refresh();
      });
	}

	addNodes(nodes, options) {

		//let max = nodes.reduce((acc, node) => 
    //  Math.max(acc, node.exclusive), 0)

    let exclusive = nodes.map((node) => node.exclusive)
    let m = median(exclusive) * 2

    console.log("Median " + (m / 2))

    nodes.forEach((node, index) => {
      //node.type = 'diamond';
      node.borderColor = '#000000';  
      node.x = Math.floor((Math.random() * this._width) + 1);
      node.y = Math.floor((Math.random() * this._height) + 1);
      if(options == null || options.size == null)
        node.size = Math.ceil(Math.max(node.exclusive, m) * 3 / (m));
      else
        node.size = options.size;
			this._sigma.graph.addNode(node)
    });
	}

/*  
  dropNodes(nodes){
    nodes.forEach((node) =>
      this._sigma.graph.dropNode(node.id)
    )
  }

  dropNodesFilter(filter){
    let nodes = this._sigma.graph.nodes().filter(filter)
    this.dropNodes(nodes)
  }
*/

  filterNodes(filter, name){
    //Note that we can chain many filters before apply
    this._filter
      .undo(name)
      .nodesBy(filter, name)
      .apply();
  }

  filterNodesByExclusiveTime(exclusive){
    this.filterNodes((node) => (node.exclusive >= exclusive), 'exclusive-filter')
  }

  filterNodesByInclusiveTime(inclusive){
    this.filterNodes((node) => (node.inclusive >= inclusive), 'inclusive-filter')
  }

	addEdges(edges) {
		edges.forEach((edge, index) =>
			this._sigma.graph.addEdge(edge)
		)
	}

  startForceDelayed(t1, t2){
    if(this._timeout != null){
      console.log("canceling previous force!")
      clearTimeout(this._timeout)
    }
    this._timeout = setTimeout(() => {this.startForce(t2);}, t1)
  }

	startForce(timeout){
    console.log("Starting force!")
		this._sigma.startForceAtlas2(
	        {
	          autoSettings: false,
	          linLogMode: true, //default
	          outboundAttractionDistribution: false,
	          adjustSizes: false,
	          edgeWeightInfluence: 0,
	          scalingRatio: 1,
	          strongGravityMode: false,
	          gravity: 1, //20 def: 1,
	          jitterTolerance: 1,
	          barnesHutOptimize: false,
	          barnesHutTheta: 0.5,//1.2 def: 0.5,
	          speed: 1,//20,
	          outboundAttCompensation: 1,
	          totalSwinging: 1,
	          totalEffectiveTraction: 0,
	          complexIntervals: 500,
	          simpleIntervals: 1000
	        }
        )

		setTimeout(()=>{
			console.log("stopping atlas"); 
			this._sigma.stopForceAtlas2();
		}, timeout);
	}
}